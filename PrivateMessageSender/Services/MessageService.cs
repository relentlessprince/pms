﻿using PrivateMessageSender.Database;
using PrivateMessageSender.Models;
using System;
using System.Linq;
using PrivateMessageSender.ViewModels;

namespace PrivateMessageSender.Services
{
    public class MessageService : IMessageService
    {
        private PmsDbContext _db;

        public MessageService(PmsDbContext db)
        {
            _db = db;
        }

        public ComposedMessage Obtain(Guid id)
        {
            var message = _db.Messages.Find(id);

            if (message == null)
                return null;

            if(ShouldBeDeleted(message))
            {
                _db.Messages.Remove(message);
                if(message.ExpiryType != ComposedMessage.ExpiryTypes.Immediately)
                {
                    _db.SaveChanges();
                    return null;
                }
            }

            message.OpenedAt = DateTime.UtcNow;
            _db.SaveChanges();

            return message;
        }

        private bool ShouldBeDeleted(ComposedMessage message)
        {
            switch (message.ExpiryType)
            {
                case ComposedMessage.ExpiryTypes.None:
                    return false;
                case ComposedMessage.ExpiryTypes.In:
                    return (DateTime.UtcNow - (message.OpenedAt ?? DateTime.UtcNow)).TotalMinutes >= message.ExpirationText;
                case ComposedMessage.ExpiryTypes.At:
                    return DateTime.UtcNow >= message.ExpirationDate;
                case ComposedMessage.ExpiryTypes.Immediately:
                    return true;
                default:
                    return false;
            }
        }
    }

    public interface IMessageService
    {
        ComposedMessage Obtain(Guid id);
    }
}
