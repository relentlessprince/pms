﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PrivateMessageSender.Models.Auth;
using PrivateMessageSender.ViewModels;

namespace PrivateMessageSender.Services
{
    public interface IAuthService
    {
        AuthData GetAuthData(Guid id);
        string HashPassword(string password);
        bool VerifyPassword(string actualPassword, string hashedPassword);
    }
}
