﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PrivateMessageSender.Database;
using PrivateMessageSender.Models;
using PrivateMessageSender.Services;
using PrivateMessageSender.ViewModels;

namespace PrivateMessageSender.Controllers
{
    [Route("api/[controller]")]
    public class MessageController : Controller
    {
        private IMessageService _messageService;
        private PmsDbContext _db;

        public MessageController(PmsDbContext db, IMessageService messageService)
        {
            _messageService = messageService;
            _db = db;
        }

        [HttpPost("[action]")]
        public IActionResult Compose([FromBody]ComposedMessage obj)
        {
            obj.Id = Guid.NewGuid();

            _db.Messages.Add(obj);
            _db.SaveChanges();

            return new JsonResult(obj);
        }

        [HttpGet("{id}")]
        public IActionResult Obtain(Guid id)
        {
            return new JsonResult(_messageService.Obtain(id));
        }
    }
}
