﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivateMessageSender.Models.Auth
{
    public interface IEntityBase
    {
        Guid Id { get; set; }
    }
}
