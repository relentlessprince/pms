﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PrivateMessageSender.Database;
using PrivateMessageSender.Models.Auth;

namespace PrivateMessageSender.Repositories
{
    public class UserRepository : EntityBaseRepository<User>, IUserRepository
    {
        public UserRepository(PmsDbContext context) : base(context) { }

        public bool IsEmailUnique(string email)
        {
            var user = this.GetSingle(u => u.Email == email);
            return user == null;
        }

        public bool IsUsernameUnique(string username)
        {
            var user = this.GetSingle(u => u.UserName == username);
            return user == null;
        }
    }
}
