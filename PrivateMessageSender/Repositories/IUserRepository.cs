﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PrivateMessageSender.Models.Auth;

namespace PrivateMessageSender.Repositories
{
    public interface IUserRepository : IEntityBaseRepository<User>
    {
        bool IsEmailUnique(string email);
        bool IsUsernameUnique(string username);
    }
}
