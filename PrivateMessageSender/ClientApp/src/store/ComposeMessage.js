﻿import { encryptMessage } from '../utils/pgp';
const updateMessageBodyType = 'UPDATE_MESSAGE_BODY';
const updateExpiryTypeType = 'UPDATE_EXPIRY_TYPE';
const updateExpirationDateType = 'UPDATE_EXPIRATION_DATE';
const updateExpirationTextType = 'UPDATE_EXPIRATION_TEXT';
const updateEncryptedType = 'UPDATE_ENCRYPTED';
const updatePublicKeyType = 'UPDATE_PUBLIC_KEY';
const submitFormType = 'SUBMIT_FORM';
const clearFormType = 'CLEAR_FORM';
export const options = [{ value: '0', label: 'Does not expire' }, { value: '1', label: 'Expires in' }, { value: '2', label: 'Expires at' }, { value: '3', label: 'Expires immediately' }];
const initialState = { message: '', expiryType: 0, expirationDate: new Date(), expirationText: 1, selectedExpiryType: options[0], isEncrypted: true, publicKey: '', guid: null };

export const actionCreators = {
  updateMessageBody: body => async (dispatch, getState) => {
    dispatch({
      type: updateMessageBodyType, body: body.target.value
    });
  },
  updateExpiryType: expiryType => async (dispatch, getState) => {
    dispatch({
      type: updateExpiryTypeType, expiryType: expiryType
    });
  },
  updateExpirationDate: expirationDate => async (dispatch, getState) => {
    dispatch({
      type: updateExpirationDateType, expirationDate: expirationDate
    });
  },
  updateExpirationText: expirationText => async (dispatch, getState) => {
    dispatch({
      type: updateExpirationTextType, expirationText: expirationText.target.value
    });
  },
  toggleEncrypted: isChecked => async (dispatch, getState) => {
    dispatch({
      type: updateEncryptedType, isEncrypted: isChecked.target.checked
    });
  },
  updatePublicKey: key => async (dispatch, getState) => {
    dispatch({
      type: updatePublicKeyType, publicKey: key.target.value
    });
  },
  onSubmit: ev => async (dispatch, getState) => {
    ev.preventDefault();
    if (getState().composeMessage.isEncrypted) {
      getState().composeMessage.message = await encryptMessage(getState().composeMessage)
    }
    const data = JSON.stringify(getState().composeMessage.message, getState().composeMessage.publicKey);

    var response = await fetch('api/Message/Compose', {
      method: 'POST',
      body: data,
      headers: { 'Content-Type': 'application/json' }
    });
    var message = await response.json();

    dispatch({ type: submitFormType, guid: message.id });
  }
};

export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === updateMessageBodyType) {
    return {
      ...state,
      message: action.body
    };
  }
  else if (action.type === updateExpiryTypeType) {
    return {
      ...state,
      expiryType: parseInt(action.expiryType.value,10),
      selectedExpiryType: action.expiryType
    };
  }
  else if (action.type === updateExpirationDateType) {
    return {
      ...state,
      expirationDate: action.expirationDate
    };
  }
  else if (action.type === updateExpirationTextType) {
    return {
      ...state,
      expirationText: action.expirationText
    };
  }
  else if (action.type === clearFormType) {
    return {
      ...initialState
    };
  }
  else if (action.type === updateEncryptedType) {
    return {
      ...state,
      isEncrypted: action.isEncrypted
    };
  }
  else if (action.type === updatePublicKeyType) {
    return {
      ...state,
      publicKey: action.publicKey
    };
  }
  else if (action.type === submitFormType) {
    return {
      ...initialState,
      guid: action.guid
    };
  }

  return state;
};
