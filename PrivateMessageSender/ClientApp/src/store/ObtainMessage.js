﻿import { decipherMessage } from '../utils/pgp';
const receiveMessageType = 'RECEIVE_MESSAGE';
const decipherMessageType = 'DECIPHER_MESSAGE';
const updatePrivateKeyType = 'UPDATE_PRIVATE_KEY';
const updatePassphraseType = 'UPDATE_PASSPHRASE';
const initialState = {
  message: {},
  privateKey: '',
  passphrase: '',
  decryptedMessage: ''
};

export const actionCreators = {
  requestMessage: guid => async (dispatch, getState) => {
    const url = `api/Message/${guid}`;
    const response = await fetch(url);
    const message = await response.json();

    dispatch({ type: receiveMessageType, message: message });
  },
  updatePrivateKey: key => async (dispatch, getState) => {
    dispatch({ type: updatePrivateKeyType, key: key.target.value });
  },
  updatePassphrase: passphrase => async (dispatch, getState) => {
    dispatch({ type: updatePassphraseType, passphrase: passphrase.target.value });
  },
  onSubmit: ev => async (dispatch, getState) => {
    ev.preventDefault();

    const message = await decipherMessage(getState().obtainMessage.message.message, getState().obtainMessage.privateKey, getState().obtainMessage.passphrase);

    dispatch({ type: decipherMessageType, decryptedMessage: message });
  },
  saveKey: () => async (dispatch, getState) => {
      localStorage.setItem('privatePgpKey', getState().obtainMessage.privateKey);
  },
  updatePrivateKeyString: key => async (dispatch, getState) => {
	  dispatch({ type: updatePrivateKeyType, key: key });
  }
};

export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === receiveMessageType) {
    return {
      ...state,
      message: action.message
    };
  }
  else if (action.type === updatePrivateKeyType) {
    return {
      ...state,
      privateKey: action.key
    };
  }
  else if (action.type === updatePassphraseType) {
    return {
      ...state,
      passphrase: action.passphrase
    };
  }
  else if (action.type === decipherMessageType) {
    return {
      ...state,
      decryptedMessage: action.decryptedMessage
    };
  }

  return state;
};
