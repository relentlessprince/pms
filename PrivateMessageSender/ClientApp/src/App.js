import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import Counter from './components/Counter';
import FetchData from './components/FetchData';
import ComposeMessage from './components/ComposeMessage';
import ObtainMessage from './components/ObtainMessage';

export default () => (
  <Layout>
    <Route exact path='/' component={Home} />
    <Route path='/counter' component={Counter} />
    <Route path='/fetch-data/:startDateIndex?' component={FetchData} />
    <Route path='/compose-message' component={ComposeMessage} />
    <Route path='/message/:guid?' component={ObtainMessage} />
  </Layout>
);
