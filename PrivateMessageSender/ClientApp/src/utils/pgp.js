﻿const openpgp = require('openpgp');

export async function encryptMessage(message, publicKey) {
  let options;

  const readableStream = new ReadableStream({
    start(controller) {
      controller.enqueue(message);
      controller.close();
    }
  });

  options = {
    message: openpgp.message.fromText(readableStream),
    publicKeys: (await openpgp.key.readArmored(publicKey)).keys
  };

  const encrypted = await openpgp.encrypt(options);
  return await openpgp.stream.readToEnd(encrypted.data);
}

export async function decipherMessage(message, privateKey, passphrase) {
  const privKeyObj = (await openpgp.key.readArmored(privateKey)).keys[0];
  await privKeyObj.decrypt(passphrase);

  const options = {
    message: await openpgp.message.readArmored(message),
    privateKeys: [privKeyObj]
  };

  const decrypted = await openpgp.decrypt(options);
  return await openpgp.stream.readToEnd(decrypted.data);
}