﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/ObtainMessage';

class ObtainMessage extends Component {
  componentDidMount() {
    // This method is called when the component is first added to the document
    this.props.requestMessage(this.props.match.params.guid);
    const privatePgpKey = localStorage.getItem('privatePgpKey');
    if (privatePgpKey) {
	    this.props.updatePrivateKeyString(privatePgpKey);
    }
  }

  componentDidUpdate() {
    // This method is called when the route parameters change
  }

  render() {
    return (
      <div>
        <h1>Weather forecast</h1>
        <p>{this.props.message.message}</p>
        <p>Your message is {this.props.message.isEncrypted ? null : "not "}encrypted and will {this.props.message.expiryType == 0 ? "never " : null}expire.</p>
        {this.props.message.isEncrypted ? renderDecryptionTool(this.props) : null}
      </div>
    );
  }
}

function renderDecryptionTool(props) {
  return (
    <div>
      <form onSubmit={props.onSubmit}>
        <textarea rows="10" value={props.privateKey} onChange={props.updatePrivateKey} />
        <input type="password" value={props.passphrase} onChange={props.updatePassphrase} />
        <input type="submit" text="Decrypt" />
        <button onClick={props.saveKey}>Save key</button>
      </form>
      <p>{props.decryptedMessage}</p>
    </div>
  );
}

export default connect(
  state => state.obtainMessage,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(ObtainMessage);
