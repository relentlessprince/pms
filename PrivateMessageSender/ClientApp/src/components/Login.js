﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/Login';

class Login extends Component {
  componentDidMount() {
  }

  componentDidUpdate() {
  }

  ensureDataFetched() {
  }

  render() {
    return (
      <div>
        <input type="text" />
        <input type="password" />
        <input type="submit" />
      </div>
    );
  }
}

export default connect(
	state => state.login,
	dispatch => bindActionCreators(actionCreators, dispatch)
)(Login);
