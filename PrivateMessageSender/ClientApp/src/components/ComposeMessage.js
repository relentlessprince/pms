﻿import 'react-datepicker/dist/react-datepicker.css';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import DatePicker from 'react-datepicker';
import { actionCreators, options } from '../store/ComposeMessage';

class ComposeMessage extends Component {
  componentDidMount() {
    // This method is called when the component is first added to the document
  }

  componentDidUpdate() {
    // This method is called when the route parameters change
  }

  render() {
    const relativeLink = `/message/${this.props.guid}`;
    return (
      <form onSubmit={this.props.onSubmit}>
        {this.props.guid !== null ? <p>Your newly created message exists on <Link tag={Link} className="text-dark" to={relativeLink}>here.</Link></p> : null}
        <h1>Compose message</h1>
        <p>This form was done to simply create and send messages over internet with possibility to adjust expiry settings and encrypt those messages with given public pgp key.</p>
        {renderMessageTextArea(this.props)}
        {renderExpiryTypeSelection(this.props)}
        {renderEncryptionWays(this.props)}
        <div class="row col-md-4 text-center">
          <input type="submit" class="btn btn-primary" />
        </div>
      </form>
    );
  }
}

function renderMessageTextArea(props) {
  return (
    <div class="row">
      <textarea class="col-md-12" rows="4" onChange={props.updateMessageBody} value={props.message} name="Message"></textarea>
    </div>
  );
}

function renderExpiryTypeSelection(props) {
  return (
    <div class="row">
      <div class="col-md-6">
        <Select value={props.selectedExpiryType} onChange={props.updateExpiryType} options={options} className="basic-single" classNamePrefix="select" />
      </div>
      <div class="col-md-6">
        {props.expiryType == 1 ? renderExpirationText(props) : props.expiryType == 2 ? renderExpirationDatePicker(props) : null}
      </div>
    </div >
  );
}

function renderExpirationDatePicker(props) {
  return (
    <DatePicker
      selected={props.expirationDate}
      onChange={props.updateExpirationDate}
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      dateFormat="dd-MM-yyyy HH:mm"
      timeCaption="Time"
      name="ExpirationDate" />
  );
}

function renderExpirationText(props) {
  return (
    <input type="number"
      onChange={props.updateExpirationText}
      min="1"
      value={props.expirationText}
      name="ExpirationText" />
  );
}

function renderEncryptionWays(props) {
  return (
    <div class="row">
      <input type="checkbox" checked={props.isEncrypted} onChange={props.toggleEncrypted} /> Encrypt with pgp key
      {props.isEncrypted ? <textarea value={props.publicKey} rows="10" onChange={props.updatePublicKey} /> : null}
    </div>
  );
}

export default connect(
  state => state.composeMessage,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(ComposeMessage);
