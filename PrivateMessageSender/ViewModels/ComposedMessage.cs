﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PrivateMessageSender.ViewModels
{
    public class ComposedMessage
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Message { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? ExpirationText { get; set; }
        public ExpiryTypes ExpiryType { get; set; }
        public DateTime? OpenedAt { get; set; }
        public bool IsEncrypted { get; set; }

        public enum ExpiryTypes
        {
            None = 0,
            In = 1,
            At = 2,
            Immediately = 3
        }
    }
}
