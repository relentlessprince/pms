﻿using Microsoft.EntityFrameworkCore;
using PrivateMessageSender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PrivateMessageSender.Models.Auth;
using PrivateMessageSender.ViewModels;

namespace PrivateMessageSender.Database
{
    public class PmsDbContext : DbContext
    {
        public PmsDbContext(DbContextOptions<PmsDbContext> options) : base(options)
        {
        }

        public DbSet<ComposedMessage> Messages { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
